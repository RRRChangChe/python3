import numpy as np

array = np.array([[1,2,3],
                [4,5,6]])

print(array)
print("num of dimension: ", array.ndim)
print("shape: ", array.shape)
print("size: ", array.size)