import numpy as np

A = np.arange(14,2,-1).reshape((3,4))
print(A)
print(np.argmin(A)) #最小值的索引
print(np.argmax(A)) #最大值的索引
print(np.mean(A))   #平均值
print(np.average(A))    #平均值
print(np.median(A)) #中位數

print(np.cumsum(A)) #逐步累加
print(np.diff(A))   #逐個累差
print(np.nonzero(A))    #非零
print(np.sort(A))   #排序

# Transportation
print(np.transpose(A))  #轉置矩陣
print(A.T)              #轉置矩陣
print(A.T.dot(A))

# Clip
print(np.clip(A,5,9))
