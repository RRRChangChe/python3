import numpy as np

# define array and dtype
arr = np.array([2,3,4], dtype=np.int64)
print(arr.dtype)

# multiple dimension
arr = np.array([[1,2,3], [2,3,4]])
print(arr.shape)

# np attribute
zero = np.zeros((3,4))
print(zero)
ones = np.ones((4,5))
print(ones)

arrange = np.arange(12,20,3)
print(arrange)
arrange = np.arange(12)
print(arrange)
arrange = arrange.reshape((3,4))
print(arrange)

linespace = np.linspace(1,10,4)
print(linespace)