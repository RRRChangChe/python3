import numpy as np

A = np.array([1,1,1])
B = np.array([2,2,2])
C = np.vstack((A,B))    # vertical stack
print(C)
C = np.hstack((A,B))    # horizontal stack
print(C)

# newaxis
print(A)
print(A.shape)
print(A.T)
print(A.T.shape)
print(A[np.newaxis,:])
print(A[np.newaxis,:].shape)
print(A[:,np.newaxis].shape)

# concatenate
A = np.array([1,1,1])[:,np.newaxis]
B = np.array([2,2,2])[:,np.newaxis]
C = np.concatenate((A,B), axis=1)
print(A)
print(B)
print(C)
C = np.concatenate((A,B,B,A), axis=1)
print(C)
