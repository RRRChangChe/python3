import numpy as np

a = np.array([[10,20,30,40]])
b = np.arange(4)
print(a,b)

# operators
c = a+b
print(c)
c = a**b
print(c)
c = 10*np.sin(a)
print(c)
print(b<3)
print(b==3)

# array operations
a = np.array([[0,1],
            [1,1]])
b = np.arange(4).reshape((2,2))
c = a*b     # 逐個相乘
print(c)
c = a.dot(b)    # 矩陣相乘
c = np.dot(a,b)    # 矩陣相乘
print(c)

# random array
a = np.random.random((2,4))
print(a)

# min, max, sum, mean
print("sum: ",np.sum(a))
print("sum: ",np.sum(a, axis=0))    # axis=0 對行取總和
print("sum: ",np.sum(a, axis=1))    # axis=1 對列取總和
print("min: ",np.min(a))
print("min: ",np.min(a, axis=1))
print("max: ",np.max(a, axis=1))
print("mean: ", np.mean(a, axis=0))
print("mean: ", np.mean(a, axis=1))
