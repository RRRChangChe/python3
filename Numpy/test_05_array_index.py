import numpy as np

# array index
A = np.arange(3,15).reshape(3,4)
print(A, 'A\n----------\n')
print(A[2,3],'A[2,3]\n----------\n')   # row3 and col4
print(A[2][3],'A[2][3]\n----------\n')  # row3 and col4
print(A[2,:],'A[2,:]\n----------\n')   # :冒號代表所有數
print(A[:,2],'A[:,2]\n----------\n')
print(A[2,1::2],'A[2,1::2]\n----------\n')
print(A[:,:],'A[:,:]\n----------\n')
print(A[:][:],'A[:][:] = A[:,:]\n----------\n')

# array in for loop
for row in A:
    print(row)
print('\n----------\n')
for col in A.T:
    print(col)
for item in A.flat:
    print(item)
    