import xml.etree.cElementTree as ET
import os, sys
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def pretty_write(elem, docname):
    xmlstr = minidom.parseString(ET.tostring(elem)).toprettyxml(indent="   ")
    with open(current_dir+'/%s.xml'%(docname), "w") as f:
        f.write(xmlstr)

# Parse XML file to the tree form
current_dir = os.path.dirname(__file__)

# Building XML documents
print('\n---------- Building XML documents ----------')
a = ET.Element('elem')
c = ET.SubElement(a, 'child1')
c.attrib = {"type":"", "value":""}
c.text = "some text"
d = ET.SubElement(a, 'child2')
b = ET.Element('elem_b')
root = ET.Element('root')
root.extend((a, b))
tree = ET.ElementTree(root)
tree.write(current_dir+'/doc2.xml')
pretty_write(root, 'doc3')
print(prettify(root))

# build a tree structure
root = ET.Element("html")
head = ET.SubElement(root, "head")
title = ET.SubElement(head, "title")
title.text = "Page Title"
body = ET.SubElement(root, "body")
body.set("bgcolor", "#ffffff")
body.text = "Hello, World!"
# wrap it in an ElementTree instance, and save as XML
tree = ET.ElementTree(root)
tree.write(current_dir+"/page.xhtml")


