import xml.etree.cElementTree as ET
import os, sys, time
from xml.dom import minidom

current_dir = os.path.dirname(__file__)

def pretty_write(elem, docname):
    xmlstr = minidom.parseString(ET.tostring(elem)).toprettyxml(indent="\t")
    with open(current_dir+'/%s.xml'%(docname), "w") as f:
        f.write(xmlstr)
    # elem.write(xmlstr)

def remove_elem(root, attrib, value):
    '''
    root : the root be iterated
    attrib : find the element with given attribute
    value : find the attribute of element with given value
    '''
    for elem in root.iter():
        # print(list(elem))
        for child in list(elem):
            if child.get(attrib) == value:
                elem.remove(child)


# Get root node element and find child node from the tree
tree = ET.ElementTree(file=current_dir+'/raw_project.xml')
root = tree.getroot()

# Create new element
workpiece = ET.Element('workpiece')
feature = ET.SubElement(workpiece, 'feature')
feature.set('number', 'W1F1')

feature2 = ET.SubElement(workpiece, 'feature')
feature2.set('number', 'W1F2')

subfeature = ET.SubElement(feature2, 'sub-feature')
subfeature.set('number', 'W1F2S1')

sub_2_feature = ET.SubElement(subfeature, 'sub-2-feature')
sub_2_feature.set('number', 'W1F2S1S1')

# Add element to the root node
root.append(workpiece)

# Remove element from root
remove_elem(root, 'number', 'W1F2S1')


print(root.findall('workpiece'))
# XML write a file
# tree.write(current_dir+'/raw_project.xml')
pretty_write(root, 'project')

