# ref: http://www.wklken.me/posts/2012/05/25/python-xml-etree.html

import xml.etree.cElementTree as ET
import os, sys
from xml.dom import minidom


def pretty_write(elem, docname):
    xmlstr = minidom.parseString(ET.tostring(elem)).toprettyxml(indent="\t")
    with open(current_dir+'/%s.xml'%(docname), "w") as f:
        f.write(xmlstr)
    # elem.write(xmlstr)


# Parse XML file to the tree form
current_dir = os.path.dirname(__file__)
tree = ET.ElementTree(file=current_dir+'/doc1.xml')

# Get root node element and find child node from the tree
root = tree.getroot()

# Create a new element
a = ET.Element('elem')
c = ET.SubElement(a, 'child1')
c.text = "some text"

# find a node and insert new element
node = tree.find('branch[@name="release01"]/sub-branch')
node.append(a)
tree.write(current_dir+'/doc1.xml')
# pretty_write(root, 'haha')

# remove a node
root.remove(a)

