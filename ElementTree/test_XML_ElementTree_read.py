"""
@version: python3.x
@author:Al
@software: VSCode
@file: xml.etree.ElementTree tutorial
@time: 2019/05/10
@ref: ref: https://pycoders-weekly-chinese.readthedocs.io/en/latest/issue6/processing-xml-in-python-with-element-tree.html
 + https://pymotw.com/2/xml/etree/ElementTree/create.html
"""
import xml.etree.cElementTree as ET
import os, sys
from xml.dom import minidom

# Parse XML file to the tree form
current_dir = os.path.dirname(__file__)
tree = ET.ElementTree(file=current_dir+'/doc1.xml')


# Get root node element and find child node from the tree 
print('\n---------- Get root node element and find child node from the tree ----------')
root = tree.getroot()
for child_of_root in root:
   print(child_of_root.tag, child_of_root.attrib)


# Find the element we interested
print('\r\n---------- Find the element we interested ----------')
for elem in tree.iter():
   print(elem.tag, elem.attrib, elem.text)

print('---------- Find the element with given tag ----------')
for elem in tree.iter(tag='branch'):
   # print(elem.tag, elem.attrib)
   print(elem.items())

# ET.dump(tree)

# Help from XPath
# Search specific element
print('\n---------- Help from XPath ----------')
print("Selects all child elements with the given tag.")
print('----------------------------------------')
for elem in tree.iterfind('branch/sub-branch'):
   print(elem.tag, elem.attrib)

print("\r\nSelects all elements that have the given attribute.")
print('----------------------------------------')
for elem in tree.iterfind('branch[@name]'):
   print(elem.tag, elem.attrib)

print("\r\nSelects all elements for which the given attribute has the given value.")
print('----------------------------------------')
for elem in tree.iterfind('branch[@name="release01"]'):
   print(elem.tag, elem.attrib)

print("\r\nSelects all child elements with given tag/attribute and value.")
print('----------------------------------------')
for elem in tree.findall('branch/sub-branch[@name="subrelease01"]'):
   print(elem.tag, elem.attrib)

# ref:https://stackoverflow.com/questions/33141235/find-all-elements-in-elementtree-by-attribute-using-python
print("\r\nSelects all child elements with unknown tag, and given attribute/value.")
print('----------------------------------------')
for elem in tree.findall('branch/*[@name="subrelease01"]'):
   print(elem.tag, elem.attrib)

print("\r\nSelects all subelements")
print('----------------------------------------')
for elem in tree.findall('.//'):
   print(elem.tag, elem.attrib)

print("\r\nSelects all subelements")
print('----------------------------------------')
def deepfind(node):
   if list(node):
      for sub_node in list(node):
         print(sub_node.tag)
         deepfind(sub_node)
   else: return

deepfind(root)

"""
https://www.itread01.com/content/1545426921.html
1、ElementTree例項代表整個XML樹，可以使用getroot()來獲取根節點。Element表示樹上的單個節點，它是
   iterable的。操作整個XML文件時使用ElementTree類，比如讀寫XML檔案。操作XML元素及其子元素時使用
   Element類。
2、xml.etree.cElementTree是用C語言實現的模組，介面上與xml.etree.ElementTree完全相同，然而處理效率
   更快，但並不是所有平臺均支援，因為我們可以嘗試匯入，若沒有就匯入ElementTree

3、每個元素包含如下屬性：
　 tag：string物件，表示資料代表的種類。
　 attrib：dictionary物件，表示附有的屬性。
　 text：string物件，表示element的內容。
　 tail：string物件，表示element閉合之後的尾跡。
　 若干子元素（child elements） 

4、查詢方法：
    Element.findall(match)方法通過tag名字或xpath匹配第一層子元素，按照子元素順序以列表形式返回所有
    匹配的元素。 
    Element.find(match)方法通過tag名字或xpath在第一層子元素中查詢第一個匹配的元素，返回匹配的元素
    或None。 
    Element.get(key, default=None)返回元素名字為key的屬性值，如果沒有找到，返回None或設定的預設值。

5、通過Element物件的方法修改Element物件
    Element.text=value可以直接修改其text屬性。
    Element.tail=value可以直接修改其tail屬性。
    Element.set(key, vlaue)可以新增新的attrib。
    Element.append(subelement)可以新增新的子元素。
    Element.extend(subelements)新增子元素的列表（引數型別是序列）。
    Element.remove(subelement)可以刪除子元素

6、使用ET.SubElement(parent, tag_name)可以快速建立子元素關係，使用ET.dump(elem)可以輸出elem的內容
   到標準輸出（elem可以是一個Element物件或ElementTree物件）
   
7、ET.parse(filename)一次性將整個XML檔案載入到記憶體，ET.iterparse(filename)採用增量形式載入XML資料，
   佔據更小的記憶體空間。
"""

"""
ET.dump(root)
print(root.tag)
print(root.attrib)
print(root.text.strip())
print(root.tail)
tree.findall('country/*')  //查詢孫子節點元素
tree.findall('.//rank')  //查詢任意層次元素
tree.findall('country[@name]')   // 包含name屬性的country
tree.findall('country[@name="Singapore"]')   // name屬性為Singapore的country
tree.findall('country[rank]')   // 孩子元素中包含rank的country
tree.findall('country[rank="1"]')   // 孩子元素中包含rank且rank元素的text為1的country
tree.findall('country[1]')     // 第一個country
tree.findall('country[last()]')   // 最後一個country
tree.findall('country[last()-1]')    // 倒數第二個country
"""