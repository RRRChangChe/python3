from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg19 import preprocess_input, decode_predictions
from tensorflow.keras.models import Model
import numpy as np

base_model = VGG19(weights='imagenet', include_top=True)
# model = Model(inputs=base_model.input, outputs=base_model.get_layer('block4_pool').output)

# img_path = 'data/ImageNet_elephant.jpg'
img_path = 'data/ImageNet_cat.png'
img = image.load_img(img_path, target_size=(224, 224))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

# block4_pool_features = model.predict(x)
block4_pool_features = base_model.predict(x)
# 取得feature 維度為(1, 14, 14, 512)
print(block4_pool_features.shape)

# 取得前三個最可能的類別及機率
print('Predicted:', decode_predictions(block4_pool_features, top=3)[0])
'''
辨識大象結果為
Predicted: [('n02504458', 'African_elephant', 0.7253825), ('n02504013', 'Indian_elephant', 0.15877835), ('n01871265', 'tusker', 0.11583838)]

辨識長姿勢貓咪結果為
Predicted: [('n04209133', 'shower_cap', 0.6492849), ('n04557648', 'water_bottle', 0.1674229), ('n03958227', 'plastic_bag', 0.100797154)]

'''