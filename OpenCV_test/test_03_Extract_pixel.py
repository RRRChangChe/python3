import numpy as np
import cv2 

# 負片效果
def access_pixels(image):
    print(image.shape)
    height = image.shape[0]
    width = image.shape[1]
    channels = image.shape[2]
    print(f"height:{height}, width:{width}, channels:{channels}")
    print(f"type(image): {type(image)}")
    # print(f"image: {image}")
    print(f"image[100,100] 取得[100,100]像素: {image[100,100]}")
    print(f"img[100,100,0] 取得[100,100]的藍色像素值: {image[100,100,0]}")
    
    # 負片效果
    for row in range(height):
        for col in range(width):
            for c in range(channels):
                pv = image[row, col, c]
                image[row, col, c] = 255 - pv
    cv2.imshow("pixel_demo", image)


def extract_blue(image):
    temp_img = np.copy(image)
    blue = temp_img[:,:,0]
    print(blue)


def swap_blue_and_red(image):
    temp_img = np.copy(image)
    temp_img[:,:,0] = image[:,:,2]
    temp_img[:,:,2] = image[:,:,0]
    cv2.imshow("original image", image)
    cv2.imshow("swap blue and red", temp_img)


img = cv2.imread('./misc/house.tiff')
# access_pixels(img)
# extract_blue(img)
swap_blue_and_red(img)

# 按下任意鍵則關閉所有視窗
cv2.waitKey(0)
cv2.destroyAllWindows()