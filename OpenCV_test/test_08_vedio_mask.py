import cv2 as cv
import numpy as np

def extract_object_demo():
    capture = cv.VideoCapture("./misc/test_08_vedio.mp4" )
    while True:
        ret, frame = capture.read()
        if ret == False:
            break

        # resize video frame
        h,w,layers = frame.shape
        frame = cv.resize(frame, (int(w/2), int(h/2)))

        # create mask
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        lower_hsv = np.array([37, 43, 46])
        upper_hsv = np.array([77, 255, 255])
        mask = cv.inRange(hsv , lowerb=lower_hsv, upperb=upper_hsv)

        # apply mask to frame
        dst = cv.bitwise_and(frame, frame, mask=mask)
        
        cv.imshow("vedio", frame)
        cv.imshow("mask", dst) 
        if cv.waitKey(40) == 27:
            break
        

extract_object_demo()