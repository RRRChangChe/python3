#%%
import cv2
import numpy as np
from matplotlib import pyplot as plt
import os

def showImg(*srcs: str):
    # print("----------Hello OpenCV--------------")
    for i in srcs:
        src = cv2.imread(i)  #ex:  "./misc/house.tiff"
        name = os.path.basename(i)
        cv2.namedWindow(name, cv2.WINDOW_AUTOSIZE)
        cv2.imshow(name, src) 

    cv2.waitKey(0)
    cv2.destroyAllWindows()


# 調整飽和度
def adjust_saturation(img: np.array, percentage:int) -> np.array:
    '''
    Reference: C:\AL\Course\cupoy_電腦視覺與深度學習馬拉松\OpenCV_CVMarathon\homework\Day003_color_spave_op_HW.ipynb
    '''
    # 將 color space 轉為 HSV
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    img_adjust = img_hsv.astype("float32")
    img_adjust[..., -1] = img_adjust[..., -1]/255 + percentage
    img_adjust = np.clip(img_adjust, 0, 255) 
    img_adjust[..., -1] = img_adjust[..., -1]*255
    img_adjust = img_adjust.astype("uint8")

    # 轉換 color space 回 BGR
    img_adjust = cv2.cvtColor(img_adjust, cv2.COLOR_HSV2BGR)

    return img_adjust


# 調整對比/明亮 gamma correction
def gamma_correction(img, gamma):
    '''
    ref: https://docs.opencv.org/3.4/d3/dc1/tutorial_basic_linear_transform.html
    '''
    lookUpTable = np.empty((1,256), np.uint8)
    for i in range(256):
        lookUpTable[0,i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
    img_gamma = cv2.LUT(img, lookUpTable)

    return img_gamma


# 調整圖片大小比例
def resizeWithAspectRatio(img, width=None, height=None, inter=cv2.INTER_AREA):
    '''
    ref: https://stackoverflow.com/questions/35180764/opencv-python-image-too-big-to-display
    '''
    (h, w) = img.shape[:2]

    if width is None and height is None:
        pass
    elif width is None:
        r = height/ float(h)
        img = cv2.resize(img, (height, int(w*r)), inter)
    else:
        r = width/ float(w)
        img = cv2.resize(img, (int(h*r), width), inter)

    return img


#%% 測試飽和度 adjust_saturation
img = cv2.imread("./data/lena.png")
img_adjust = adjust_saturation(img, -0.5)

img_RGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img_adjust_RGB = cv2.cvtColor(img_adjust, cv2.COLOR_BGR2RGB)
img_contact = np.hstack([img_RGB, img_adjust_RGB])
plt.imshow(img_contact)

#%% 測試 gamma correction 
img = cv2.imread("./data/lena.png")
img_adjust = gamma_correction(img, 0.5)

img_RGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img_adjust_RGB = cv2.cvtColor(img_adjust, cv2.COLOR_BGR2RGB)
img_contact = np.hstack([img_RGB, img_adjust_RGB])
plt.imshow(img_contact)

# %% 測試圖片大小比例
img = cv2.imread("./data/lena.png")
img_resize = resizeWithAspectRatio(img, height=256)

plt.subplot(1,2,1), plt.title("original")
plt.imshow(img)
plt.subplot(1,2,2), plt.title("resize")
plt.imshow(img_resize)
plt.show()
# %%
