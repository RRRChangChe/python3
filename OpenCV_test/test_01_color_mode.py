import cv2

# color mode 
img = cv2.imread("./misc/house.tiff", cv2.IMREAD_GRAYSCALE)
cv2.imshow("GRAY", img)
cv2.waitKey(0)

img = cv2.imread("./misc/house.tiff", cv2.IMREAD_COLOR)
cv2.imshow("COLOR", img)
cv2.waitKey(0)

img = cv2.imread("./misc/house.tiff", cv2.IMREAD_UNCHANGED)
cv2.imshow("UNCHANGED", img)
cv2.waitKey(0)

cv2.destroyAllWindows()
