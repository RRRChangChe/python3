import numpy as np 
import cv2
import time

npTmp = np.random.random((1024, 1024)).astype(np.float32)

npMat1 = np.stack([npTmp, npTmp], axis=2)
npMat2 = npMat1

cuMat1 = cv2.cuda_GpuMat()
cuMat2 = cv2.cuda_GpuMat()
cuMat1.upload(npMat1)
cuMat2.upload(npMat2)

ti = time.time()
cv2.cuda.gemm(cuMat1, cuMat2, 1, None, 0, None, 1)
print(f"CUDA --- {time.time() - ti} seconds")

ti = time.time()
cv2.gemm(npMat1, npMat2, None, 0, None, 1)
print(f"cpu --- {time.time() - ti} seconds")


# # C:\msys64\mingw64\bin
# C:\AL\Software\OpenCV_build\opencv-4.5.5\modules\core\include\opencv2/core/utils/filesystem.private.hpp(1,1): warning C
# 4819: 檔案含有無法在目前字碼頁 (950) 中表示的字元。請以 Unicode 格式儲存檔案以防止資料遺失 (正在編譯原始程式檔 C:\AL\Software\OpenCV_build\opencv-4.5.5\modules\co
# re\src\ocl.cpp) [C:\AL\Software\OpenCV_build\build\modules\world\opencv_world.vcxproj]