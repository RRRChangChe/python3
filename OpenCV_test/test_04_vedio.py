import cv2 as cv
import numpy as np


def vedio_demo():
    capture = cv.VideoCapture(0)    # or file path 
    while True:
        ret, frame = capture.read()
        frame = cv.flip(frame, 1)
        cv.imshow('vedio', frame)
        c = cv.waitKey(50)
        if c == 27:
            break


if __name__ == "__main__":
    vedio_demo()
    cv.waitKey(0)
    cv.destroyAllWindows()