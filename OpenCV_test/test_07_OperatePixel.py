import cv2 as cv
import numpy as np
from utility import *

# 像素相加
def add_demo(src1, src2):
    src = cv.add(src1, src2)
    cv.imshow("add_demo", src)

# 像素相減
def substract_demo(src1, src2):
    src = cv.subtract(src1, src2)
    cv.imshow("substract_demo", src)

# 像素相乘
def multiply_demo(src1, src2):
    src = cv.multiply(src1, src2)
    cv.imshow("multiply_demo", src)

# 像素相除
def divide_demo(src1, src2):
    src = cv.divide(src1, src2)
    cv.imshow("divide_demo", src)

# 像素均值
def mean_demo(src1, src2):
    m1, dev1 = cv.meanStdDev(src1)  # 均值, 方差
    m2, dev2 = cv.meanStdDev(src2)

    print(m1)
    print(m2)
    print(dev1)
    print(dev2)

# 像素邏輯運算: 與或非
def logic_demo(src1, src2):
    # and: true and false = false
    and_Img = cv.bitwise_and(src1, src2)
    cv.imshow("And", and_Img)

    # or: true or false = true
    or_Img = cv.bitwise_or(src1, src2)
    cv.imshow("OR", or_Img)

    # not: not true = false 負片效果
    not_Img = cv.bitwise_not(src1)
    cv.imshow("NOT", not_Img)

def contrast_brightness_demo(src, c, b):
    '''
    src, 對比度, 亮度值+n
    '''
    h, w, ch = src.shape
    blank = np.zeros([h, w, ch], src.dtype)
    dst = cv.addWeighted(src, c, blank, 1-c, b)
    cv.imshow("contrast_brightness_demo", dst)


src1 = cv.imread("./opencv-master/samples/data/WindowsLogo.jpg")
src2 = cv.imread("./opencv-master/samples/data/LinuxLogo.jpg")
# cv.namedWindow("image1", cv.WINDOW_AUTOSIZE)
# cv.imshow("image1", src1)
# cv.imshow("image2", src2)

# 像素相加
# add_demo(src1, src2)

# 像素相減
# substract_demo(src1, src2)
# substract_demo(src2, src1) # 觀察結果不一樣

# 像素相乘
# multiply_demo(src1, src2)

# 像素相除
# divide_demo(src1, src2) 
# divide_demo(src2, src1)   # 觀察結果不一樣

# 像素均值
# mean_demo(src1, src2) 

# 像素邏輯運算: 與或非
# logic_demo(src1, src2)

# 對比度/ 亮度
src = cv.imread("./opencv-master/samples/data/lena.jpg")
cv.imshow("image1", src)
contrast_brightness_demo(src, 1.2, 50) # src, 對比度, 亮度值+n

cv.waitKey(0)
cv.destroyAllWindows()