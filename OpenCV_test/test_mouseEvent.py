import cv2

def onMouse(event, x, y, flags, param):
    # print("Enter onMouse function")
    image_copy = image.copy()
    if event == cv2.EVENT_MOUSEMOVE and x is not None and y is not None:
        image_point = cv2.putText(image_copy, f"{x}, {y}", (0, 20), cv2.FONT_HERSHEY_COMPLEX, fontScale=1, color=(0,255,0))
        cv2.imshow("image", image_point)
    if event == cv2.EVENT_LBUTTONDOWN:
        image_point = cv2.circle(image_copy, (x,y), 3, (0,255,0), 3)
        cv2.imshow("image", image_point)

# config cv2 window
cv2.namedWindow("image")
cv2.moveWindow("image", 100, 0)
cv2.setMouseCallback('image', onMouse)

image = cv2.imread("data\dartboard.jpg")
cv2.imshow("image", image)
cv2.waitKey(0)