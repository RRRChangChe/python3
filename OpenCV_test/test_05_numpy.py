# coding: utf-8
# data/ dtype/ size/ shape/ len
import cv2 as cv
import numpy as np

def access_pixels(image):
    print(image.shape)
    height = image.shape[0]
    width = image.shape[1]
    channels = image.shape[2]
    print(f"width:{width}, height:{height}, channels:{channels}")


def create_image():
    # img = np.zeros([400, 400, 1], np.uint8)
    # # img[:,:,0] = np.ones([400,400])*177 // or
    # img = img * 255
    # cv.imshow("image3", img)
    
    img = np.ones([3,3], np.uint8)
    img.fill(122.288)
    print(img)

# set current workplace directory
# import os 
# os.chdir(os.path.dirname(__file__))
src = cv.imread("./misc/house.tiff")
cv.namedWindow("input image", cv.WINDOW_AUTOSIZE)
cv.imshow("input image", src) 
access_pixels(src)
create_image()
cv.waitKey(0)
