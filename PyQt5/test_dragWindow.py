# https://www.davex.pw/2016/03/25/PyQt-Learning-I/

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import QtWidgets

class Demo(QtWidgets.QWidget):
    def __init__(self, parent = None):
        super(Demo, self).__init__(parent)

        
    
    def mousePressEvent(self, event):
        if event.button()==Qt.LeftButton:
            self.m_drag=True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()

    def mouseMoveEvent(self, QMouseEvent):
        if QMouseEvent.buttons() and Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_drag=False


def main():
   app = QtWidgets.QApplication(sys.argv)
   ex = Demo()
   ex.show()
   sys.exit(app.exec_())
	

if __name__ == '__main__':
   main()

