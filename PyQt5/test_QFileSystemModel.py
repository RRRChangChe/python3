from PyQt5 import QtGui, QtCore, QtWidgets
import sys

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    #Window系统提供的模式
    model = QtWidgets.QFileSystemModel()
    model.setReadOnly(False)
    model.setNameFilters(["*.lmm"])
    model.setNameFilterDisables(False)
    rootIndex = model.setRootPath("D:/AL")
    # model.setNameFilters(".lmm;")
    #创建一个QtreeView部件
    tree = QtWidgets.QTreeView()
    #为部件添加模式
    tree.setModel(model)
    tree.setWindowTitle(tree.tr("Dir View"))
    tree.resize(640, 480)
    tree.expand(rootIndex)
    tree.scrollTo(rootIndex)
    tree.setCurrentIndex(rootIndex)
    #隱藏header中項目
    header = tree.header()
    header.setSectionsMovable(True)
    header.setDefaultSectionSize(400)
    tree.hideColumn(1) #隱藏size
    tree.hideColumn(2) #隱藏type
    tree.show()
    sys.exit(app.exec_())