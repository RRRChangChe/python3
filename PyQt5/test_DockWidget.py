# https://www.tutorialspoint.com/pyqt/pyqt_qdockwidget.htm

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import QtWidgets

class Dockdemo(QtWidgets.QMainWindow):
   def __init__(self, parent = None):
      super(Dockdemo, self).__init__(parent)
		
      layout = QtWidgets.QHBoxLayout()
      bar = self.menuBar()
      file = bar.addMenu("File")
      file.addAction("New")
      file.addAction("save")
      file.addAction("quit")
		
      self.items = QtWidgets.QDockWidget("Dockable", self)
      self.listWidget = QtWidgets.QListWidget()
      self.listWidget.addItem("item1")
      self.listWidget.addItem("item2")
      self.listWidget.addItem("item3")
		
      self.items.setWidget(self.listWidget)
      self.items.setFloating(False)
      self.setCentralWidget(QtWidgets.QTextEdit())
      self.addDockWidget(Qt.RightDockWidgetArea, self.items)
      self.setLayout(layout)
      self.setWindowTitle("Dock demo")


class DockDemo_layout(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(DockDemo_layout, self).__init__(parent)
        self.setCentralWidget(QtWidgets.QTextEdit())

        self.docked = QtWidgets.QDockWidget("Dockable", self)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.docked)
        self.dockedWidget = QtWidgets.QWidget(self)
        self.docked.setWidget(self.dockedWidget)
        self.setWindowTitle("Dock demo")
        self.dockedWidget.setLayout(QtWidgets.QVBoxLayout())
        for i in range(5):
            self.dockedWidget.layout().addWidget(QtWidgets.QPushButton("{}".format(i)))


def main():
   app = QtWidgets.QApplication(sys.argv)
   ex = Dockdemo()
   ex2 = DockDemo_layout()
   ex.show()
   ex2.show()
   sys.exit(app.exec_())
	
if __name__ == '__main__':
   main()