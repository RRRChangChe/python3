# -*- coding:Big5 -*-
import sys, os
from PyQt5 import QtCore
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import numpy as np

# class LineNumberArea(QWidget):
#     def __init__(self, editor):
#         super().__init__()
#         self.editor = editor


#     def sizeHint(self):
#         return QSize(self.editor.lineNumberAreaWidth(), 0)


#     def paintEvent(self, event):
#         print('LineNumberArea.paintEvent')
#         self.editor.lineNumberAreaPaintEvent(event)


# class CodeEditor(QPlainTextEdit):
#     def __init__(self):
#         super().__init__()
#         self.lineNumberArea = LineNumberArea(self)

#         self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
#         self.updateRequest.connect(self.updateLineNumberArea)
#         self.cursorPositionChanged.connect(self.highlightCurrentLine)

#         self.updateLineNumberAreaWidth(0)


#     def lineNumberAreaWidth(self):
#         """ This method has been slightly modified (use of log and uses actual
#         font rather than standart.) """
#         n_lines = self.blockCount()
#         digits = np.ceil(np.log10(n_lines)) + 1
#         print(n_lines, ' ', digits)
#         block = self.firstVisibleBlock()
#         print('block number: ', block.blockNumber())
#         return digits * QFontMetrics(self.font()).width('9') + 3


#     def updateLineNumberAreaWidth(self, _):
#         print('CodeEditor.updateLineNumberAreaWidth: margin = {}'.format(self.lineNumberAreaWidth()))
#         self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)


#     def updateLineNumberArea(self, rect, dy):
#         print('CodeEditor.updateLineNumberArea: rect = {}, dy = {}'.format(rect, dy))

#         if dy:
#             self.lineNumberArea.scroll(0, dy)
#         else:
#             self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(),
#                                        rect.height())

#         print('CodeEditor.updateLineNumberArea: rect.contains(self.viewport().rect()) = {}'.format(rect.contains(self.viewport().rect())))
#         if rect.contains(self.viewport().rect()):
#             self.updateLineNumberAreaWidth(0)


#     def resizeEvent(self, event):
#         super().resizeEvent(event)

#         cr = self.contentsRect()
#         self.lineNumberArea.setGeometry(QRect(cr.left(), cr.top(),
#                                         self.lineNumberAreaWidth(), cr.height()))

#     def lineNumberAreaPaintEvent(self, event):
#         print('CodeEditor.lineNumberAreaPaintEvent')
#         QPainter(self.lineNumberArea)
#         QPainter.fillRect(event.rect(), Qt.lightGray)

#         block = self.firstVisibleBlock()
#         blockNumber = block.blockNumber()
#         print('block number: ', blockNumber)
#         top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
#         bottom = top + self.blockBoundingRect(block).height()

#         # Just to make sure I use the right font
#         height = QFontMetrics(self.font()).height()
#         while block.isValid() and (top <= event.rect().bottom()):
#             if block.isVisible() and (bottom >= event.rect().top()):
#                 number = str(blockNumber + 1)
#                 QPainter.setPen(Qt.black)
#                 QPainter.drawText(0, top, lineNumberArea.width(), height,
#                                  Qt.AlignRight, number)

#             block = block.next()
#             top = bottom
#             bottom = top + self.blockBoundingRect(block).height()
#             blockNumber += 1


#     def highlightCurrentLine(self):
#         extraSelections = []

#         if not self.isReadOnly():
#             selection = QTextEdit.ExtraSelection()

#             lineColor = QColor(Qt.yellow).lighter(160)

#             selection.format.setBackground(lineColor)
#             selection.format.setProperty(QTextFormat.FullWidthSelection, True)
#             selection.cursor = self.textCursor()
#             selection.cursor.clearSelection()
#             extraSelections.append(selection)
#         self.setExtraSelections(extraSelections)


# if __name__ == "__main__":
#     app = QApplication(sys.argv)

#     txt = CodeEditor()
#     txt.show()

#     sys.exit(app.exec_())




class QCodeEditor(QPlainTextEdit):

    class NumberBar(QWidget):

        def __init__(self, editor):
            QWidget.__init__(self, editor)
    
            self.editor = editor
            self.editor.blockCountChanged.connect(self.updateWidth)
            self.editor.updateRequest.connect(self.updateContents)
            self.font = QFont()
            self.numberBarColor = QColor("#e8e8e8")
    
        def paintEvent(self, event):
    
            painter = QPainter(self)
            painter.fillRect(event.rect(), self.numberBarColor)
    
            block = self.editor.firstVisibleBlock()
        
            while block.isValid():
                blockNumber = block.blockNumber()
                block_top = self.editor.blockBoundingGeometry(block).translated(self.editor.contentOffset()).top()
                if blockNumber == self.editor.textCursor().blockNumber():
                    self.font.setBold(True)
                    painter.setPen(QColor("#000000"))
                else:
                    self.font.setBold(False)
                    painter.setPen(QColor("#717171"))
    
                paint_rect = QRect(0, block_top, self.width(), self.editor.fontMetrics().height())
                painter.drawText(paint_rect, Qt.AlignCenter, str(blockNumber+1))
    
                block = block.next()
                
        def getWidth(self):  
            count = self.editor.blockCount()
            if 0 <= count < 99999:
                width = self.fontMetrics().width('99999') 
            else:
                width = self.fontMetrics().width(str(count))
            return width      
    
        def updateWidth(self):
            width = self.getWidth()
            self.editor.setViewportMargins(width, 0, 0, 0)
        
        def updateContents(self, rect, dy):
            if dy:
                self.scroll(0, dy)
            else:
                self.update(0, rect.y(), self.width(), rect.height())
            if rect.contains(self.editor.viewport().rect()):   
                fontSize = self.editor.currentCharFormat().font().pointSize()
                self.font.setPointSize(fontSize)
                self.font.setStyle(QFont.StyleNormal)
                self.updateWidth()
    

    def __init__(self):        

        super(QCodeEditor, self).__init__()
        self.setWindowTitle('TextEditor')

        self.setFont(QFont("Ubuntu Mono", 12))
        self.setLineWrapMode(QPlainTextEdit.NoWrap)
        self.number_bar = self.NumberBar(self)           
        self.currentLineNumber = None
        self.cursorPositionChanged.connect(self.highligtCurrentLine)    
        self.setViewportMargins(40, 0, 0, 0)
        self.highligtCurrentLine()

    def resizeEvent(self, *e):
        pass

    def highligtCurrentLine(self):
        extraSelections = []
        if not self.isReadOnly():
            selection = QTextEdit.ExtraSelection()

            lineColor = QColor(Qt.yellow).lighter(160)

            selection.format.setBackground(lineColor)
            selection.format.setProperty(QTextFormat.FullWidthSelection, True)
            selection.cursor = self.textCursor()
            selection.cursor.clearSelection()
            extraSelections.append(selection)
        self.setExtraSelections(extraSelections)

        
if __name__ == "__main__":
    app = QApplication(sys.argv)

    txt = QCodeEditor()
    txt.show()

    sys.exit(app.exec_())