from PySide import QtCore, QtGui
import xml.etree.cElementTree as ET

def parseXMLtotree(root, item=None):
    def addSubItemtoItem(root, item=None):
        if root.getchildren():
            for elem in root:
                subItem = QtGui.QTreeWidgetItem(item)
                if elem.attrib: 
                    subItem.setText(0, elem.attrib['type'])
                    subItem.setText(1, elem.attrib['value'])
                else: 
                    subItem.setText(0, elem.tag)
                addSubItemtoItem(elem, subItem)
        else: return

    if root.getchildren():
        for elem in root:
            subItem = QtGui.QTreeWidgetItem(tw)
            subItem.setFont(0, QtGui.QFont("PMingLiU",-1,QtGui.QFont.Bold))
            # add widget into item
            # pb = QtGui.QPushButton("test")
            # tw.setItemWidget(subItem, 0, pb)
            if elem.attrib.has_key("type"): 
                subItem.setText(0, elem.attrib['type'])
                subItem.setText(1, elem.attrib['value'])
            else: 
                subItem.setText(0, elem.tag)
            addSubItemtoItem(elem, subItem)
    else: return


class InventoryDelegate(QtGui.QStyledItemDelegate):
    def paint(self, painter, option, index):
        super(InventoryDelegate, self).paint(painter, option, index)
        if not index.parent().isValid():
            painter.save()
            painter.setPen(QtGui.QPen(QtGui.QColor(204,204,204)))
            r = QtCore.QRect(option.rect)
            r.adjust(0, 1, 0, -1)
            painter.drawLine(r.topLeft(), r.topRight())
            # painter.drawLine(r.bottomLeft(), r.bottomRight())
            painter.restore()

    def sizeHint(self, option, index):
        s = super(InventoryDelegate, self).sizeHint(option, index)
        s.setHeight(55)
        return s

# Create TreeWidget
tw = QtGui.QTreeWidget()
tw.setColumnCount(2)
tw.setHeaderLabels(['Property','Value'])
tw.header().setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
delegate = InventoryDelegate(tw)
tw.setItemDelegate(delegate)

# Parse XML to tree
fpath = r'D:\FreeCAD_0.17.12764_x64_dev_win\FreeCAD_0.17.12764_x64_dev_win\Mod\Wheel\wheelPackage\test\Config.xml'
tree = ET.ElementTree(file=fpath)
root = tree.getroot()
parseXMLtotree(root) 
tw.show()
