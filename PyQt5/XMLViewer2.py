import sip
sip.setapi('QString', 2)

from xml.etree import cElementTree as etree
from PyQt5 import QtCore, QtGui, QtXml, QtWidgets

class Window(QtWidgets.QWidget):
    def __init__(self, xml):
        QtWidgets.QWidget.__init__(self)
        self.tree = QtWidgets.QTreeWidget(self)
        self.tree.header().hide()
        self.importTree(xml)
        self.button = QtWidgets.QPushButton('Export', self)
        self.button.clicked.connect(self.exportTree)
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.tree)
        layout.addWidget(self.button)

    def importTree(self, xml):
        def build(item, root):
            for element in root.getchildren():
                child = QtWidgets.QTreeWidgetItem(
                    item, [element.attrib['text']])
                child.setFlags(
                    child.flags() | QtCore.Qt.ItemIsEditable)
                build(child, element)
            item.setExpanded(True)
        root = etree.fromstring(xml)
        build(self.tree.invisibleRootItem(), root)

    def exportTree(self):
        def build(item, root):
            for row in range(item.childCount()):
                child = item.child(row)
                element = etree.SubElement(
                    root, 'node', text=child.text(0))
                build(child, element)
        root = etree.Element('root')
        build(self.tree.invisibleRootItem(), root)
        from xml.dom import minidom
        print(minidom.parseString(etree.tostring(root)).toprettyxml())

if __name__ == '__main__':

    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = Window("""\
<?xml version="1.0" ?>
<root>
    <node text="Child (0)">
        <node text="Child (0)">
            <node text="Child (0)"/>
            <node text="Child (1)"/>
        </node>
        <node text="Child (1)">
            <node text="Child (0)"/>
            <node text="Child (1)"/>
        </node>
    </node>
    <node text="Child (1)">
        <node text="Child (0)">
            <node text="Child (0)"/>
            <node text="Child (1)"/>
        </node>
        <node text="Child (1)">
            <node text="Child (0)"/>
            <node text="Child (1)"/>
        </node>
    </node>
</root>
        """)
    window.setGeometry(800, 300, 300, 300)
    window.show()
    sys.exit(app.exec_())