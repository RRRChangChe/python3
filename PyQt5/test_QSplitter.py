# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\FreeCAD_0.17.12764_x64_dev_win\FreeCAD_0.17.12764_x64_dev_win\Mod\Wheel\wheelPackage\test\Combo_View.ui'
#
# Created: Wed Sep 18 15:29:22 2019
#      by: pyside2-uic 2.0.0 running on PySide2 5.6.0~a1
#
# WARNING! All changes made in this file will be lost!
import sys
from PySide2.QtWidgets import QApplication, QDialog, QSplitter
from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(523, 629)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.treeView = QtWidgets.QTreeView(Form)
        self.treeView.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.treeView.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.treeView.setLineWidth(1)
        self.treeView.setUniformRowHeights(False)
        self.treeView.setAnimated(True)
        self.treeView.setObjectName("treeView")
        # self.verticalLayout.addWidget(self.treeView)
        self.tabWidget = QtWidgets.QTabWidget(Form)
        self.tabWidget.setFocusPolicy(QtCore.Qt.TabFocus)
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.South)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Triangular)
        self.tabWidget.setMovable(True)
        self.tabWidget.setTabBarAutoHide(False)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setContentsMargins(0, 6, 0, 0)
        self.gridLayout_2.setHorizontalSpacing(9)
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.tab)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.gridLayout_2.addWidget(self.plainTextEdit, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.gridLayout = QtWidgets.QGridLayout(self.tab_2)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setContentsMargins(0, 6, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.listView = QtWidgets.QListView(self.tab_2)
        self.listView.setFrameShape(QtWidgets.QFrame.Box)
        self.listView.setObjectName("listView")
        self.gridLayout.addWidget(self.listView, 1, 0, 1, 1)
        self.tabWidget.addTab(self.tab_2, "")
        # self.verticalLayout.addWidget(self.tabWidget)

        self.splitter = QSplitter(Form)
        self.splitter.addWidget(self.treeView)
        self.splitter.addWidget(self.tabWidget)
        self.splitter.setOrientation(QtCore.Qt.Vertical)    # Qt.Vertical 垂直   Qt.Horizontal 水平
        self.verticalLayout.addWidget(self.splitter)

        handle = self.splitter.handle(1)
        
        self.retranslateUi(Form)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Combo View", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QtWidgets.QApplication.translate("Form", "GCode", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QtWidgets.QApplication.translate("Form", "Property", None, -1))


app = QApplication(sys.argv)
label = QDialog()
label.ui = Ui_Form()
label.ui.setupUi(label)
label.show()
app.exec_()