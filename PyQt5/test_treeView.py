from PyQt5 import QtGui, QtCore, QtWidgets
import sys

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    #Window系统提供的模式
    model = QtWidgets.QDirModel()
    #创建一个QtreeView部件
    tree = QtWidgets.QTreeView()
    #为部件添加模式
    tree.setModel(model)
    tree.setWindowTitle(tree.tr("Dir View"))
    tree.resize(640, 480)
    tree.show()
    sys.exit(app.exec_())


# myTreeWidget = new QTreeWidget(this);
#     QStringList headers;
#     headers << "內容" << "編號";
#     myTreeWidget->setHeaderLabels(headers); //設定QTreeWidget的標頭  
#     myTreeWidget->setWindowTitle(tr("QTreeWidget"));
#     QTreeWidgetItem *root1 = new QTreeWidgetItem(myTreeWidget, QStringList()<<QString("根節點1")<<"0");
#     QTreeWidgetItem *root2 = new QTreeWidgetItem(myTreeWidget, QStringList()<<QString("根節點2")<<"3");
#     QTreeWidgetItem *leaf1 = new QTreeWidgetItem(root1, QStringList()<<QString("子節點1")<<"1");
#     QTreeWidgetItem *leaf2 = new QTreeWidgetItem(root1, QStringList()<<QString("子節點2")<<"2");
#     leaf2->setCheckState(0, Qt::Checked);   //設定此子節點有可勾選的選項

#     QList<QTreeWidgetItem *> rootList;
#     rootList << root1 << root2;   //將所有根節點存入容器
#     myTreeWidget->insertTopLevelItems(0, rootList);  //設定此容器內容為QTreeWidget的根節點
#     myTreeWidget->resize(250,200);


# tw = cfg.d.dockWidgetContents
# ti = QtGui.QTreeWidgetItem(tw)
# ti.setText(0,'root1')
# ti2 = QtGui.QTreeWidgetItem(tw)
# ti2.setText(0,'root2')
# l = [ti, ti2]
# tw.addTopLevelItems(l)
# # or tw.insertTopLevelItem(0, ti)
# leaf1 = QtGui.QTreeWidgetItem(ti)
# leaf1.setText(0,"leaf1")
# leaf2 = QtGui.QTreeWidgetItem(ti2)
# leaf2.setText(0,"leaf2")
# leaf2.setCheckState(0, QtCore.Qt.Checked)   #設定此節點有可勾選的選項

# # tw.setDragDropMode(QtGui.QAbstractItemView.DragDrop)
# # tw.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
# # https://stackoverflow.com/questions/34055060/get-drag-n-drop-qtreewidget-items-python

