# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'd:\AL\Usync\Python 3\PyQt5\Program_Viewer.ui',
# licensing of 'd:\AL\Usync\Python 3\PyQt5\Program_Viewer.ui' applies.
#
# Created: Fri Jan  3 10:54:40 2020
#      by: pyside2-uic  running on PySide2 5.13.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets
import uic, os

UIpath = os.path.join(os.path.dirname(__file__), 'test_treeWidget_dragNdrop.ui')

class Program_Viewer_UI(QtWidgets.QDialog, uic.loadUiType(UIpath)[0]):
    def __init__(self, parent=None):
        super(Program_Viewer_UI, self).__init__(parent)
        self.setupUi(self)
        self.createItem()

        self.pushButton.setText(u"↑")
        self.pushButton_2.setText(u"↓")
        self.pushButton.clicked.connect(self.on_pushButton_click)
        self.pushButton_2.clicked.connect(self.on_pushButton_2_click)

    def createItem(self):
        item1 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        item1.setText(0, 'item1')
        item1child1 = QtWidgets.QTreeWidgetItem(item1)
        item1child1.setText(0, "item1child1")
        item1child2 = QtWidgets.QTreeWidgetItem(item1)
        item1child2.setText(0, "item1child2")

        item2 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        item2.setText(0, 'item2')
        item2child1 = QtWidgets.QTreeWidgetItem(item2)
        item2child1.setText(0, "item2child1")
        item2child2 = QtWidgets.QTreeWidgetItem(item2)
        item2child2.setText(0, "item2child2")

        item3 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        item3.setText(0, 'item3')
        item3child1 = QtWidgets.QTreeWidgetItem(item3)
        item3child1.setText(0, "item3child1")
        item3child2 = QtWidgets.QTreeWidgetItem(item3)
        item3child2.setText(0, "item3child2")

    def on_pushButton_click(self):
        item = self.treeWidget.currentItem()
        row = self.treeWidget.currentIndex().row()

        if item and (not item.parent()) and row > 0:
            self.treeWidget.takeTopLevelItem(row)
            self.treeWidget.insertTopLevelItem(row-1, item)
            self.treeWidget.setCurrentItem(item)

    def on_pushButton_2_click(self):
        item = self.treeWidget.currentItem()
        row = self.treeWidget.currentIndex().row()

        if item and (not item.parent()) and (self.treeWidget.topLevelItemCount()-1 > row >= 0):
            self.treeWidget.takeTopLevelItem(row)
            self.treeWidget.insertTopLevelItem(row+1, item)
            self.treeWidget.setCurrentItem(item)

if __name__ == '__main__':
    import sys
    from PySide2.QtWidgets import QApplication
    app = QApplication(sys.argv)
    d = Program_Viewer_UI()
    d.show()
    sys.exit(app.exec_())