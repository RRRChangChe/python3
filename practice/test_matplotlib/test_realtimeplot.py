import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import serial, threading
from queue import Queue

# initialize serial port reading
COMPORT = "COM3"
ser = serial.Serial(COMPORT)

# initialize matplotlib for real time data plotting
# fig, ax = plt.subplots()
data = Queue()
xdata, ydata = [], []

def thread_job(data):
    while True:
        value = ser.readline()
        try:
            value = int(value.strip(b"\r\n"))
            data.put(value)
            print(list(data.queue))
        except: print("baad")

def update(frame):
    ydata = list(data.queue)
    xdata = list(range(len(ydata)))
    if len(ydata) > 500: data.get()

    print(f"{len(xdata)}, {len(ydata)}")
    plt.cla()
    plt.plot(xdata, ydata)


thread1 = threading.Thread(target=thread_job, args=(data,))
thread1.start()

ani = FuncAnimation(plt.gcf(), update, interval = 1)
plt.tight_layout()
plt.show()


    