import threading 
from queue import Queue

class Clerk:
    def __init__(self):
        self.product = -1
        self.cond = threading.Condition()

    def purchase(self, product: int):
        with self.cond:
            while self.product != -1:
                self.cond.wait()
            self.product = product
            self.cond.notify()

    def sellout(self) -> int:
        with self.cond:
            while self.product == -1:
                self.cond.wait()
            p = self.product
            self.product = -1
            self.cond.notify()
            return p

def producer(clerk: Clerk):
    for product in range(10):
        clerk.put(product)
        print('店員進貨 %s'%(product))

def consumer(clerk: Clerk):
    for _ in range(10):
        print("店員賣出 %s"%(clerk.get()))
    
# clerk = Clerk()
clerk: Queue = Queue(1)
threading.Thread(target = producer, args = (clerk,)).start()
threading.Thread(target = consumer, args = (clerk,)).start()