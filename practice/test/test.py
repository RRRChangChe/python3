s = input()
scorelist = [int(i) for i in s.split()]	# 存分數list
enumlist =[(index, score) for index, score in enumerate(scorelist)]	# 轉換成enumerate列舉陣列, 會幫每個項目給index
newEnumlist = sorted(enumlist, key = lambda i : i[1])	# 用內建sorted函式, key丟lambda函式告訴sorted用每個項目中第2個值排序
outputlist = [str(index) for index, score in newEnumlist]	# 取出排序過index重新組成outputlist，並轉成string
print(" ".join(outputlist))