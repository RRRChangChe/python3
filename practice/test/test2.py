'''
List pass by reference in module - Python 
I'm working on my side project with Python 2.7 and I was confused by the list behavior in python module. I already know python list is "pass by reference". But it doesn't work when I put it into module and call it outside. I don't know whether it's caused by the reference problem or the namespace issue in python module.

Here's the sample code.
[TEST 1]
I create a empty list named list_1 and create a list equal to list_1 named list_2. When I append a number into list_1 and list_2 turns out the same value like code below. The list works "pass by reference" perfectly.

list_1 = []
list_2 = list_1
list_1.append(1)
print(list_1)
print(list_2)

the output is:

[1]
[1]

'''
from package import myModule 
myModule.list_1.append(1)
print(id(myModule.list_1))
print(id(myModule.list_2))
print(id(myModule.dict_1['list3']))
print(myModule.list_1 == myModule.list_2)