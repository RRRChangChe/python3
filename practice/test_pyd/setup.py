from distutils.core import setup, Extension
from Cython.Build import cythonize

setup(
  name = 'Hello world app',
  ext_modules = cythonize("main.py"),
)

# # add number of files to build
# extensions = [Extension("*", ["*.py"])]

# setup(
#   name = 'Hello world app',
#   ext_modules = cythonize(extensions),
# )