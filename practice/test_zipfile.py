import zipfile
import os
 
def Achive_Folder_To_ZIP(sFilePath, dest = ""):
    """
    input : Folder path and name
    output: using zipfile to ZIP folder
    """
    if (dest == ""):
        zf = zipfile.ZipFile(sFilePath + '.ZIP', mode='w')
    else:
        zf = zipfile.ZipFile(dest, mode='w')
 
    os.chdir(sFilePath)
    #print sFilePath
    for root, folders, files in os.walk(".\\"):
        for sfile in files:
            aFile = os.path.join(root, sfile)
            #print aFile
            zf.write(aFile)
    return zf
 
 
if __name__ == "__main__":
    zf = Achive_Folder_To_ZIP("C:\\Users\\usync\\Desktop\\testZip", "C:\\Users\\usync\Desktop\\testZip.zip")
    
    # open file in ZIP
    fp = zf.open(zf.infolist()[0].filename)

    zf2 = zipfile.ZipFile("C:\\Users\\usync\\Desktop\\123.FCStd", mode='r')
    fp = zf2.open(zf2.infolist()[0].filename)
    zf2.close()

    ## FreeCAD saveAs
    #App.getDocument("Unnamed").saveAs(u"C:/Users/usync/Desktop/123.FCStd")

    ## Save Image 
    # Gui.activeDocument().activeView().getSize()
    # Gui.activeDocument().activeView().saveImage('C:/Users/usync/Desktop/123.bmp',983,816,'Current')

    # Python create folder
    # import os
    # if not os.path.exists(r'C:\Users\usync\Desktop\my_folder'):
    #     os.makedirs(r'C:\Users\usync\Desktop\my_folder') 

    # Python remove folder
    # os.rmdir(r'C:\Users\usync\Desktop\my_folder') # delete empty directory