section1 = input("請輸入第一區開獎號碼(,分隔): ")
section2 = input("請輸入第二區開獎號碼: ")

section1 = section1.split(",")
totalPrize = 0
total = 0
while True:
    num1 = input("請輸入第一區號碼(,分隔):")
    num1 = num1.split(",")
    num2 = input("請輸入第二區號碼:")
    match1 = set(section1) & set(num1)
    if len(match1) == 1 and num2 == section2:
        totalPrize += 100
        total += 1
        print(f"恭喜中普獎100$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 3:
        totalPrize += 100
        total += 1
        print(f"恭喜中玖獎100$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 2 and num2 == section2:
        totalPrize += 200
        total += 1
        print(f"恭喜中捌獎200$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 3 and num2 == section2:
        totalPrize += 400
        total += 1
        print(f"恭喜中柒獎400$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 4:
        totalPrize += 800
        total += 1
        print(f"恭喜中陸獎800$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 4 and num2 == section2:
        totalPrize += 4000
        total += 1
        print(f"恭喜中伍獎4000$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 5:
        totalPrize += 20000
        total += 1
        print(f"恭喜中肆獎20000$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 5 and num2 == section2:
        totalPrize += 150000
        total += 1
        print(f"恭喜中參獎150000$ : {match1}, {num2}\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
    elif len(match1) == 6:
        total += 1
        print(f"恭喜中貳獎---$ : {match1}, {num2}\n")
        print(f"累積獎金: 不用看了反正很多錢啦!\n")
    elif len(match1) == 5 and num2 == section2:
        total += 1
        print(f"恭喜中頭獎---$ : {match1}, {num2}\n")
        print(f"累積獎金: 不用看了可以去訂跑車了\n")
    else:
        total += 1
        print("哭哭沒中!\n")
        print(f"累積獎金: {totalPrize}$, 累積張數: {total}\n")
