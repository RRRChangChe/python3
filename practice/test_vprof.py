from vprof import runner

def foo(n1,n2):
    l = []
    for i in range(n1):
        n = (n1**n2)**(1/n1*n2)
        l.append(n)

runner.run(foo, 'cmhp', args=(100, 20), host='localhost', port=8000)